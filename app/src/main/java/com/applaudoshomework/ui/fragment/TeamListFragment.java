package com.applaudoshomework.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.applaudoshomework.R;
import com.applaudoshomework.ui.adapter.TeamListAdapter;
import com.applaudoshomework.model.Team;
import com.applaudoshomework.ui.activity.MainActivity;

import java.util.ArrayList;

public class TeamListFragment extends Fragment {

    private SelectedTeamItem mImplementation;
    public static int positionItem;

    public interface SelectedTeamItem {
        void selectedItem(int position);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mImplementation = (SelectedTeamItem) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + "must implement SelectedTeamItem.");
        }
    }

    private TeamListAdapter loadDataWithAdapter() {
        final int[] logos = {
                R.drawable.arsenal, R.drawable.astonvilla
                , R.drawable.burnley, R.drawable.chelsea
                , R.drawable.crystalpalace, R.drawable.everton
                , R.drawable.hullcity, R.drawable.leicestercity
                , R.drawable.liverpool, R.drawable.manchestercity};
        final String[] titles = getActivity().getResources().getStringArray(R.array.team_list);
        final String[] addresses = getActivity().getResources().getStringArray(R.array.team_addresses);

        ArrayList<Team> teamArrayList = new ArrayList<>();

        for (int i = 0; i < titles.length; i++) {
            teamArrayList.add(new Team(logos[i], titles[i], addresses[i]));
        }

        return new TeamListAdapter(getContext(), teamArrayList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container
            , Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ListView teamListView = (ListView) view.findViewById(R.id.teamList);

        teamListView.setAdapter(loadDataWithAdapter());

        teamListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mImplementation.selectedItem(position);
                positionItem = position;
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity activity = (MainActivity) getActivity();
        if (activity != null) {
            activity.hideUpButton();
        }
        getActivity().setTitle(R.string.homeworkTitle);
    }

}
