package com.applaudoshomework.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.applaudoshomework.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class DetailFragment extends Fragment implements OnMapReadyCallback {

    //when rotating these 2 variables save the instance state and app does not restart
    private static final String mKEY_POSITION = "position";
    private int mCURRENT_POSITION = -1;

    //view elements
    private ImageView mImageView;
    private VideoView mVideoView;
    private TextView mTeamNameView, mTeamDescriptionView;
    private GoogleMap mMap;

    //final values cuz' they don't change
    private final LatLng mArsenal = new LatLng(51.554888, -0.108438);
    private final LatLng mAstonVilla = new LatLng(52.509896, -1.885852);
    private final LatLng mBurnley = new LatLng(53.789167, -2.230278);
    private final LatLng mChelsea = new LatLng(51.481667, -0.191111);
    private final LatLng mCrystalPalace = new LatLng(51.39828, -0.085485);
    private final LatLng mEverton = new LatLng(53.438787, -2.966319);
    private final LatLng mHullCity = new LatLng(53.746522, -0.367692);
    private final LatLng mLeicesterCity = new LatLng(52.620044, -1.1415);
    private final LatLng mLiverpool = new LatLng(53.430829, -2.96083);
    private final LatLng mManchesterCity = new LatLng(53.483138, -2.200395);

    public static DetailFragment getInstance(int position) {
        DetailFragment fragment = new DetailFragment();

        Bundle arguments = new Bundle();
        arguments.putInt(mKEY_POSITION, position);

        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //when rotating these 2 variables save the instance state and app does not restart
        if (savedInstanceState != null) {
            mCURRENT_POSITION = savedInstanceState.getInt(mKEY_POSITION);
        }

        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        mImageView = (ImageView) view.findViewById(R.id.imageTeamDetail);
        mVideoView = (VideoView) view.findViewById(R.id.videoDetail);
        mTeamNameView = (TextView) view.findViewById(R.id.teamNameDetail);
        mTeamDescriptionView = (TextView) view.findViewById(R.id.teamDescription);

        Toast.makeText(getContext(), R.string.mapMessage, Toast.LENGTH_LONG).show();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        switch (TeamListFragment.positionItem) {
            case 0:
                addMarker(mArsenal, getResources().getString(R.string.arsenal));
                moveCameraMarker(mArsenal);
                break;
            case 1:
                addMarker(mAstonVilla, getResources().getString(R.string.astonVilla));
                moveCameraMarker(mAstonVilla);
                break;
            case 2:
                addMarker(mBurnley, getResources().getString(R.string.burnley));
                moveCameraMarker(mBurnley);
                break;
            case 3:
                addMarker(mChelsea, getResources().getString(R.string.chelsea));
                moveCameraMarker(mChelsea);
                break;
            case 4:
                addMarker(mCrystalPalace, getResources().getString(R.string.crystalPalace));
                moveCameraMarker(mCrystalPalace);
                break;
            case 5:
                addMarker(mEverton, getResources().getString(R.string.everton));
                moveCameraMarker(mEverton);
                break;
            case 6:
                addMarker(mHullCity, getResources().getString(R.string.hullCity));
                moveCameraMarker(mHullCity);
                break;
            case 7:
                addMarker(mLeicesterCity, getResources().getString(R.string.leicesterCity));
                moveCameraMarker(mLeicesterCity);
                break;
            case 8:
                addMarker(mLiverpool, getResources().getString(R.string.liverpool));
                moveCameraMarker(mLiverpool);
                break;
            case 9:
                addMarker(mManchesterCity, getResources().getString(R.string.manchesterCity));
                moveCameraMarker(mManchesterCity);
                break;
        }
    }

    private void moveCameraMarker(LatLng latLng) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
    }

    private void addMarker(LatLng latLng, String title) {
        mMap.addMarker(new MarkerOptions().position(latLng)).setTitle(title);
    }

    public void updateFragmentView(int position) {
        MediaController mediaController = new MediaController(getContext());
        mediaController.setMediaPlayer(mVideoView);
        mediaController.setAnchorView(mVideoView);
        mVideoView.setMediaController(mediaController);

        int imageTeamDetail = 0;
        String teamNameDetail = "";
        String teamDescriptionDetail = "";
        String videoPath1 = "http://clips.vorwaerts-gmbh.de/VfE_html5.mp4";
        String videoPath2 = "http://techslides.com/demos/sample-videos/small.mp4";

        switch (position) {
            case 0:
                imageTeamDetail = R.drawable.arsenal;
                teamNameDetail = getResources().getString(R.string.arsenal);
                teamDescriptionDetail = getResources().getString(
                        R.string.arsenalDescription);
                mVideoView.setVideoPath(videoPath1);
                break;
            case 1:
                imageTeamDetail = R.drawable.astonvilla;
                teamNameDetail = getResources().getString(R.string.astonVilla);
                teamDescriptionDetail = getResources().getString(
                        R.string.astonDescription);
                mVideoView.setVideoPath(videoPath2);
                break;
            case 2:
                imageTeamDetail = R.drawable.burnley;
                teamNameDetail = getResources().getString(R.string.burnley);
                teamDescriptionDetail = getResources().getString(
                        R.string.burnleyDescription);
                mVideoView.setVideoPath(videoPath1);
                break;
            case 3:
                imageTeamDetail = R.drawable.chelsea;
                teamNameDetail = getResources().getString(R.string.chelsea);
                teamDescriptionDetail = getResources().getString(
                        R.string.chelseaDescription);
                mVideoView.setVideoPath(videoPath2);
                break;
            case 4:
                imageTeamDetail = R.drawable.crystalpalace;
                teamNameDetail = getResources().getString(R.string.crystalPalace);
                teamDescriptionDetail = getResources().getString(
                        R.string.crystalDescription);
                mVideoView.setVideoPath(videoPath1);
                break;
            case 5:
                imageTeamDetail = R.drawable.everton;
                teamNameDetail = getResources().getString(R.string.everton);
                teamDescriptionDetail = getResources().getString(
                        R.string.evertonDescription);
                mVideoView.setVideoPath(videoPath2);
                break;
            case 6:
                imageTeamDetail = R.drawable.hullcity;
                teamNameDetail = getResources().getString(R.string.hullCity);
                teamDescriptionDetail = getResources().getString(
                        R.string.hullDescription);
                mVideoView.setVideoPath(videoPath1);
                break;
            case 7:
                imageTeamDetail = R.drawable.leicestercity;
                teamNameDetail = getResources().getString(R.string.leicesterCity);
                teamDescriptionDetail = getResources().getString(
                        R.string.leicesterDescription);
                mVideoView.setVideoPath(videoPath2);
                break;
            case 8:
                imageTeamDetail = R.drawable.liverpool;
                teamNameDetail = getResources().getString(R.string.liverpool);
                teamDescriptionDetail = getResources().getString(
                        R.string.liverpoolDescription);
                mVideoView.setVideoPath(videoPath1);
                break;
            case 9:
                imageTeamDetail = R.drawable.manchestercity;
                teamNameDetail = getResources().getString(R.string.manchesterCity);
                teamDescriptionDetail = getResources().getString(
                        R.string.manchesterDescription);
                mVideoView.setVideoPath(videoPath2);
                break;
        }

        mImageView.setImageDrawable(ContextCompat.getDrawable(getContext(), imageTeamDetail));
        mTeamNameView.setText(teamNameDetail);
        mTeamDescriptionView.setText(teamDescriptionDetail);
        mVideoView.start();

        getActivity().setTitle(R.string.detail);
    }

    @Override
    public void onStart() {
        super.onStart();

        Bundle arguments = getArguments();
        if (arguments != null) {
            updateFragmentView(arguments.getInt(mKEY_POSITION));
        } else if (mCURRENT_POSITION != -1) {
            updateFragmentView(mCURRENT_POSITION);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(mKEY_POSITION, mCURRENT_POSITION);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(R.string.detail);
    }

}