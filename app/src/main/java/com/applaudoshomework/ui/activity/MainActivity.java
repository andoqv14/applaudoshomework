package com.applaudoshomework.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

import com.applaudoshomework.R;
import com.applaudoshomework.ui.fragment.DetailFragment;
import com.applaudoshomework.ui.fragment.TeamListFragment;

public class MainActivity extends AppCompatActivity implements TeamListFragment.SelectedTeamItem {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (findViewById(R.id.container) != null) {
            if (savedInstanceState != null) {
                return;
            }

            TeamListFragment fragment = new TeamListFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();
        }
    }

    @Override
    public void selectedItem(int position) {
        DetailFragment fragment = (DetailFragment)
                getSupportFragmentManager().findFragmentById(
                        R.id.details_frag);
        if (fragment != null) {
            fragment.updateFragmentView(position);
        } else {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, DetailFragment
                            .getInstance(position))
                    .addToBackStack(null)
                    .commit();
        }
    }

    public void hideUpButton() {
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        SubMenu sm = menu.addSubMenu(R.string.share)
                .setIcon(android.R.drawable.ic_menu_share);
        sm.clearHeader();
        sm.add(0, 1, 1, R.string.facebook).setIcon(R.drawable.ic_icon_facebook);
        sm.add(0, 2, 2, R.string.instagram).setIcon(R.drawable.ic_icon_instagram);
        sm.add(0, 3, 3, R.string.google_plus).setIcon(R.drawable.ic_icon_google_plus);
        sm.add(0, 4, 4, R.string.twitter).setIcon(R.drawable.ic_icon_twitter);

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.home:
                startActivity(new Intent(this, MainActivity.class));
                break;
            case 1:
                newCustomTab(1);
                break;
            case 2:
                newCustomTab(2);
                break;
            case 3:
                newCustomTab(3);
                break;
            case 4:
                newCustomTab(4);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void newCustomTab(int option) {
        String url = "https://";
        switch (option) {
            case 1:
                url += "facebook.com";
                break;
            case 2:
                url += "instagram.com";
                break;
            case 3:
                url += "plus.google.com";
                break;
            case 4:
                url += "twitter.com";
                break;
        }

        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(this, Uri.parse(url));
    }

}
