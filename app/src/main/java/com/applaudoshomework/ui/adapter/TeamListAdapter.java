package com.applaudoshomework.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.applaudoshomework.R;
import com.applaudoshomework.model.Team;

import java.util.List;

public class TeamListAdapter extends ArrayAdapter<Team> {

    public TeamListAdapter(Context context, List<Team> objects) {
        super(context, R.layout.item_rv_list, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());

        @SuppressLint({"ViewHolder", "InflateParams"})
        View item = inflater.inflate(R.layout.item_rv_list, null);

        ImageView teamListImage = (ImageView) item.findViewById(R.id.imageList);
        TextView teamTitle = (TextView) item.findViewById(R.id.teamTitle);
        TextView teamAddress = (TextView) item.findViewById(R.id.teamAddress);

        teamListImage.setImageResource(getItem(position).getTeamLogo());
        teamTitle.setText(getItem(position).getTeamTitle());
        teamAddress.setText(getItem(position).getTeamAddress());

        return item;
    }

}
