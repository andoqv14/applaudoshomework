package com.applaudoshomework.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Team implements Parcelable {

    private final int mTeamLogo;
    private final String mTeamTitle;
    private final String mTeamAddress;

    public Team(int mTeamLogo, String teamTitle, String teamAddress) {
        this.mTeamLogo = mTeamLogo;
        this.mTeamTitle = teamTitle;
        this.mTeamAddress = teamAddress;
    }

    public int getTeamLogo() {
        return mTeamLogo;
    }

    public String getTeamTitle() {
        return mTeamTitle;
    }

    public String getTeamAddress() {
        return mTeamAddress;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mTeamLogo);
        dest.writeString(this.mTeamTitle);
        dest.writeString(this.mTeamAddress);
    }

    private Team(Parcel in) {
        this.mTeamLogo = in.readInt();
        this.mTeamTitle = in.readString();
        this.mTeamAddress = in.readString();
    }

    public static final Parcelable.Creator<Team> CREATOR = new Parcelable.Creator<Team>() {
        @Override
        public Team createFromParcel(Parcel source) {
            return new Team(source);
        }

        @Override
        public Team[] newArray(int size) {
            return new Team[size];
        }
    };

}
